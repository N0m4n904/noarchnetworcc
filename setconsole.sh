#!/bin/bash -e

set -e

sudo cat <<EOF > /etc/vconsole.conf
KEYMAP=us
FONT=ter-v16b
EOF