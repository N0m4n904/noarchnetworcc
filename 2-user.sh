#!/usr/bin/env bash
#-------------------------------------------------------------------------------------
# ______                          _     ______                                        
#|  ___ \         /\             | |   |  ___ \       _                               
#| |   | | ___   /  \   ____ ____| | _ | |   | | ____| |_ _ _ _  ___   ____ ____ ____ 
#| |   | |/ _ \ / /\ \ / ___) ___) || \| |   | |/ _  )  _) | | |/ _ \ / ___) ___) ___)
#| |   | | |_| | |__| | |  ( (___| | | | |   | ( (/ /| |_| | | | |_| | |  ( (__( (___ 
#|_|   |_|\___/|______|_|   \____)_| |_|_|   |_|\____)\___)____|\___/|_|   \____)____)
#                                                                                     
#-------------------------------------------------------------------------------------

echo -e "\nINSTALLING AUR SOFTWARE\n"
# You can solve users running this script as root with this and then doing the same for the next for statement. However I will leave this up to you.

echo "CLONING: YAY"
cd ~
git clone "https://aur.archlinux.org/yay.git"
cd ${HOME}/yay
makepkg -si --noconfirm
cd ~
touch "$HOME/.cache/zshhistory"
git clone https://gitlab.com/xdevs23/ah-yes-zsh.git
cd ah-yes-zsh
./install.sh
ln -s "$HOME/zsh/.zshrc" $HOME/.zshrc

PKGS=(
'autojump'
'awesome-terminal-fonts'
'dxvk-bin' # DXVK DirectX to Vulcan
'mangohud' # Gaming FPS Counter
'mangohud-common'
'nerd-fonts-fira-code'
'noto-fonts-emoji'
'papirus-icon-theme'
'ocs-url' # install packages from websites
'sddm-nordic-theme-git'
'snapper-gui-git'
'ttf-droid'
'ttf-hack'
'ttf-meslo' # Nerdfont package
'ttf-roboto'
'zoom' # video conferences
'snap-pac'
'tmux'
'oh-my-zsh-git'
# 'linux-nitrous'
# 'linux-nitrous-headers'
'gnome-shell-extension-dash-to-dock'
'gnome-shell-extension-caffeine-git'
)

for PKG in "${PKGS[@]}"; do
    yay -S --noconfirm $PKG
done



export PATH=$PATH:~/.local/bin
cp -r $HOME/NoArchNetworcc/dotfiles/* $HOME/.config/
pip install konsave
konsave -i $HOME/NoArchNetworcc/kde.knsv
sleep 1
konsave -a kde

echo -e "\nDone!\n"
exit
